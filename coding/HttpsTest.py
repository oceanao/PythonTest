# -*- coding:utf-8 -*-
import unittest
import  json
from Https import Https
import os
from htmloutput.htmloutput import HtmlOutput
from nose import run
class MyTestCase(unittest.TestCase):


#测试put请求
    def test_put(self):
        data = {
            "createUserId": 1,
            "name": "string",
            "pageNum": 1,
            "pageSize": 20,
            "sex": "string",
            "updateUserId": 1
        };
        hps=Https('http://obe.ienai.xin:10080/TaskType/list/',{'content-type': 'application/json'})
        j = hps.put(data)
        code = json.loads(j)['code']
        msg = json.loads(j)['msg']
        self.assertEqual(msg, u"查询成功")
#测试get请求
    def test_get(self):
        data = {

        };
        hps=Https('http://obe.ienai.xin:10080/TaskType/get/1',{'content-type': 'application/json'})
        j = hps.get(data)
        code = json.loads(j)['code']
        msg = json.loads(j)['msg']
        self.assertEqual(msg, u"查询成功")
#测试delete请求
    def test_delete(self):
        data = {

        };
        hps=Https('http://obe.ienai.xin:10080/TaskType/del/143',{'content-type': 'application/json'})
        j = hps.delete(data)
        code = json.loads(j)['code']
        msg = json.loads(j)['msg']
        self.assertEqual(msg, u"删除成功")


#测试post请求
    def test_post(self):
        data = {
            "details": "string",
            "id": 0,
            "name": "string"
        }
        hps = Https('http://obe.ienai.xin:10080/TaskType/add/', {'content-type': 'application/json'})
        j=hps.post(data)
        code= json.loads(j)['code']
        msg=json.loads(j)['msg']
        self.assertEqual(msg,u"新增成功")


path= os.path.dirname(__file__)
outfile = os.path.join(path, 'httpsTest.py')
run(argv=['nosetests', '-v','--with-html-output','--html-out-file=result.html',outfile],plugins=[HtmlOutput()])

if __name__ == '__main__':
    unittest.main()
