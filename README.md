# PythonTest

#### 项目介绍
Python +nose  利用requests和unittest 进行接口测试，生成测试报告

使用方法

运行
nosetests -v D:\pyproject\https\httpsTest.py （注：修改到对应的路径和名称）
 
运行截图

![运行过程](https://images.gitee.com/uploads/images/2018/0712/195113_23f35ceb_1328530.png "屏幕截图.png")
![测试报告摘要](https://images.gitee.com/uploads/images/2018/0712/194655_decea9de_1328530.png "屏幕截图.png")
![详细描述](https://images.gitee.com/uploads/images/2018/0712/195356_2e6702aa_1328530.png "屏幕截图.png")
