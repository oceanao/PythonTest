# -*- coding:utf-8 -*-
import requests
import  json
class Https(object):
  url=''
  header=''
  data=''
  def __init__(self,url,header):
      self.url=url
      self.header=header

  def post(self,data):
      r = requests.post(self.url,data=json.dumps(data), headers=self.header);
      return  r.text

  def put(self,data):
      r = requests.put(self.url,data=json.dumps(data), headers=self.header);
      return  r.text

  def get(self,data):
      r = requests.get(self.url,data=json.dumps(data), headers=self.header);
      return  r.text
  def delete(self,data):
      r = requests.delete(self.url,data=json.dumps(data), headers=self.header);
      return  r.text